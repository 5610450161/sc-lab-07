
public class Seat {
	private int time;
	private char row;
	private int collum;
	private int price;
	
	public Seat(char row,int collum,Integer time,int price){
		this.time = time;
		this.row = row;
		this.collum = collum;
		this.price = price;
	}
	
	public int getTime(){
		return this.time;
	}
	
	public char getRow(){
		return this.row;
	}
	
	public int getCollum(){
		return this.collum;
	}
	
	public int getPrice(){
		return this.price;
	}
	
	public boolean equals(Object e){
		boolean t = false;
		if(((Seat)e).getCollum() == this.collum && ((Seat)e).getRow() == this.row && ((Seat)e).getTime() == this.time && ((Seat)e).getPrice() == this.price){
			t = true;
		}
		System.out.println(t);
		return t;
		
	}
}
